class Categoria {

    constructor(nombre) {
        this.nombre = nombre;
    }
}

class Nota {

    constructor(id, titulo, contenido, categoria, modificacion) {
        this.id = id;
        this.titulo = titulo;
        this.contenido = contenido;
        this.categoria = categoria;
        this.modificacion = modificacion;
    }

    set setId(id) {
        this.id = id;
    }

    get getId() {
        return this.id;
    }

    set setTitulo(titulo) {
        this.titulo = titulo;
    }

    get getTitulo() {
        return this.telefono;
    }
}


function generarId(length) {
    var id = '';
    var caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var caracteresLength = caracteres.length;
    for (var i = 0; i < length; i++) {
        id += caracteres.charAt(Math.floor(Math.random() * caracteresLength));
    }
    return id;
}

function prueba(cantidad) {


    let id = generarId(cantidad)
    return id
}

function fechaHoy() {
    var hoy = new Date()
    var fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear();
    var hora = hoy.getHours() + 'hs-' + hoy.getMinutes() + 'min';
    var fechaHora = fecha + ' ' + hora;

    return fechaHora;
}

function llenarCategorias() {

    let categorias = JSON.parse(localStorage.getItem('categorias'));

    let option = "";
    for (let index = 0; index < categorias.length; index++) {
        let categoria = categorias[index];

        option += ' <option>' + categoria.nombre + '</option>';
    }
    document.getElementById('categoria').innerHTML = option;
    document.getElementById('modCategoria').innerHTML = option;

}

function agregarNota() {
    event.preventDefault();
    let id = generarId(20)
    let titulo = document.getElementById('titulo');
    let contenido = document.getElementById('contenido');
    let categoria = document.getElementById('categoria');
    let modificacion = fechaHoy();

    //Verifico que estén todos los campos
    if (titulo.value != "" && contenido.value != "") {
        if (titulo.value.length < 10) {
            if (contenido.value.length < 30) {


                let nuevoNota = new Nota(
                    id,
                    titulo.value,
                    contenido.value,
                    categoria.value,
                    modificacion
                );
                let notas = JSON.parse(localStorage.getItem('notas'));
                if (!notas) {
                    notas = [];
                }
                notas.push(nuevoNota);
                localStorage.setItem('notas', JSON.stringify(notas));

                // Limpio el formulario
                titulo.value = "";
                contenido.value = "";
                location.reload();
            } else {
                alert("contenido demasiado largo")
            }

        } else {
            alert("titulo demasiado largo")
        }
    } else {
        alert("complete todos los campos")
    }
}

function filtrarNota(categoria) {
    // console.log(categoria.toUpperCase());



    if (event.type == "submit") {
        event.preventDefault();
    }
    let notas = JSON.parse(localStorage.getItem('notas'));
    // Controlo que se haya ingresado algo en el buscador
    // y que la agenda no este vacía
    if (categoria && notas) {
        let notasFiltradas = notas.filter(nota => nota.categoria.toLowerCase().indexOf(categoria.toLowerCase()) > -1);
        listarNotas(notasFiltradas);
    } else {
        listarNotas(agenda);
    }
}
function listarNotas(notas = null) {

    if (notas != null) {

        document.getElementById("limpiarFiltro").style = "block"

    }

    if (notas == null) {

        let limpiar = document.getElementById("limpiarFiltro")
        limpiar.setAttribute("style", "display: none")

        notas = JSON.parse(localStorage.getItem('notas'));
        //Controlo si no tengo aún notas almacenadas
        if (!notas) {
            notas = [];
        }
    }




    //Genero el contenido de la tabla
    let tabla = "";
    for (let index = 0; index < notas.length; index++) {
        let nota = notas[index];
        tabla += '<tr><td data-toggle="modal" data-target="#staticBackdrop" onclick="detalleNota(\'' + nota.titulo + '\', \'' + nota.contenido + '\', \'' + nota.categoria + '\',  \'' + nota.modificacion + '\', \'' + nota.id + '\' )"><div class="d-block">' + '<div>' + nota.titulo + '</div> ' + '<div class="text-light">' + nota.categoria + '</div>' + '</div></td>';
    }

    //Muestro el contenido de la tabla
    document.getElementById('tablaNotas').innerHTML = tabla;
}

function detalleNota(titulo, contenido, categoria, modificacion, id) {
    document.getElementById('modalTituloNota').innerHTML = titulo;
    document.getElementById('modalCategoriaNota').innerHTML = "Categoria:  " + categoria;
    document.getElementById('modalContenidoNota').innerHTML = contenido;
    document.getElementById('modalModificacionNota').innerHTML = "Ultima Modificacion:  " + modificacion;
    let buttonE = "";
    let buttonM = "";
    buttonE += '<button type="button" class="btn btn-danger" onclick="eliminarNota(\'' + id + '\')" >Eliminar</button>';
    buttonM += '<button type="button" class="btn btn-success" onclick="cargarFormMod(\'' + id + '\')" data-toggle="modal" data-target="#modificarNota" data-dismiss="modal">Modificar</button>';
    document.getElementById('botonEliminar').innerHTML = buttonE;
    document.getElementById('botonModificar').innerHTML = buttonM;

}


function eliminarNota(id) {

    let notas = JSON.parse(localStorage.getItem('notas'));
    let indice = notas.findIndex(nota => nota.id == id);

    notas.splice(indice, 1);
    localStorage.setItem('notas', JSON.stringify(notas));
    location.reload();

}

function cargarFormMod(id) {
    llenarCategorias();
    let notas = JSON.parse(localStorage.getItem('notas'));
    let nota = notas.find(n => n.id == id);
    document.getElementById('modTitulo').value = nota.titulo
    document.getElementById('modContenido').value = nota.contenido
    document.getElementById('modCategoria').value = nota.categoria
    let button = "";
    button += '<button type="button" class="btn btn-danger" data-dismiss="modal" >Cancelar</button>';
    button += '<button type="button" class="btn btn-success" onclick="modificarNota(\'' + id + '\')" data-dismiss="modal">Modificar</button>';
    document.getElementById('botonesMod').innerHTML = button;

}

function modificarNota(id) {
    let titulo = document.getElementById('modTitulo').value;
    let contenido = document.getElementById('modContenido').value;
    let categoria = document.getElementById('modCategoria').value;
    let modificacion = fechaHoy();

    let notas = JSON.parse(localStorage.getItem('notas'));
    let index = notas.findIndex(n => n.id == id);
    notas[index].titulo = titulo;
    notas[index].contenido = contenido;
    notas[index].categoria = categoria;
    notas[index].modificacion = modificacion;
    localStorage.setItem('notas', JSON.stringify(notas));
    location.reload();


}

function crearCategoria() {
    // event.preventDefault();
    let nombre = document.getElementById('nombreCategoria');

    //Verifico que estén todos los campos
    if (nombre.value != "") {
        let nuevaCategoria = new Categoria(
            nombre.value
        );
        let categorias = JSON.parse(localStorage.getItem('categorias'));
        if (!categorias) {
            categorias = [];
        }
        categorias.push(nuevaCategoria);
        localStorage.setItem('categorias', JSON.stringify(categorias));

        // Limpio el formulario
        nombre.value = "";

    }
    listarCategorias();
}

function listarCategorias() {
    let categorias = JSON.parse(localStorage.getItem('categorias'));
    //Controlo si no tengo aún notas almacenadas
    if (!categorias) {
        categorias = [];
    }
    //Genero el contenido de la tabla
    let tabla = "";
    for (let index = 0; index < categorias.length; index++) {
        let categoria = categorias[index];
        tabla += '<tr><td class="d-flex">' + categoria.nombre;

        tabla += '<button type="button" class="ml-auto btn btn-primary  btn-sm" data-dismiss="modal" onClick="filtrarNota(\'' + categoria.nombre + '\')" >Ver Notas</button>';
        tabla += '<button type="button" class="ml-2 btn btn-danger btn-sm" onclick="eliminarCategoria(\'' + categoria.nombre + '\')">Eliminar</button></td>';

    }
    //Muestro el contenido de la tabla
    document.getElementById('tablaCategorias').innerHTML = tabla;
}

function eliminarCategoria(nombre) {

    let notas = JSON.parse(localStorage.getItem('notas'));
    let index = notas.find(n => n.categoria == nombre);
    console.log(index)

    if (index) {
        alert("Hay notas asociadas a la categoria")
    } else {

        var result = confirm("estas seguro de eliminar?");
        if (result == true) {
            let categorias = JSON.parse(localStorage.getItem('categorias'));
            let indice = categorias.findIndex(categoria => categoria.nombre == nombre);
            categorias.splice(indice, 1);
            localStorage.setItem('categorias', JSON.stringify(categorias));
            listarCategorias();
        } else {
            doc = "Cancel was pressed.";
        }
    }


}

function ocultarSpiner() {
    document.getElementById('spiner').style.display = 'none';

}

function buscarNota() {

    // document.getElementById('spiner').style.display = 'block';
    // setTimeout(ocultarSpiner(),5000);



    let notas = JSON.parse(localStorage.getItem('notas'));

    var pal = document.getElementById("buscar").value;
    let tabla = "";

    for (let index = 0; index < notas.length; index++) {
        var titulo = notas[index].titulo;
        var contenido = notas[index].contenido;
        if (pal.length != 0 && titulo.length != 0) {
            if (titulo.toLowerCase().search(pal.toLowerCase()) != -1 || contenido.toLowerCase().search(pal.toLowerCase()) != -1) {

                let nota = notas[index];
                tabla += '<tr><td class="d-flex">' + "Titulo: " + nota.titulo + " <br>contenido: " + nota.contenido + '</td>';

                document.getElementById('tablaBusqueda').innerHTML = tabla;
            }
        }
        else {
            document.getElementById('tablaBusqueda').innerHTML = tabla;

        }
    }
}

function cambio() {
    let body = document.getElementById("body")
    body.setAttribute("class", "")
}